# Accept the Go version for the image to be set as a build argument.
# Default to Go 1.14
ARG GO_VERSION=1.14

FROM golang:${GO_VERSION}

RUN apt-get update
RUN apt-get install python3 python3-dev python3-pip unzip -y
RUN pip3 install pip setuptools -U
RUN pip3 install aws-sam-cli -U

ADD https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip awscliv2.zip
RUN unzip awscliv2.zip
RUN ./aws/install
RUN rm awscliv2.zip

RUN sam --version
RUN aws --version
